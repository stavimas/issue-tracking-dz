#ifndef ORDER_STORAGE_H
#define ORDER_STORAGE_H
#include "Order.h"
#include <string>
#include <vector>

using std::string;

namespace Shop
 {
	class OrderStorage
	{
	public:
		OrderStorage();
		OrderStorage(string path);
		
		void Write();
		void Read();
		
		void Add(Order order);
		void Remove(int index);
		
		size_t GetSize();

		Order& operator[](int index);
		
	private:
		string m_path{};
		vector <Order> m_orderList{};
	};
}

#endif // !ORDER_STORAGE_H