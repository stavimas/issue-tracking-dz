#include "OrderHistory.h"
#include <iostream>
#include <iomanip>

using std::cout;
using std::cin;

namespace Shop
{
	void OrderHistory(OrderStorage& orders)
	{
		unsigned short exit = 1;
		string number{};
		cout << "������� ��� ����� �������� -> ";
		cin.ignore();
		getline(cin, number);

		unsigned short code{};
		cout << "\n������� ��� �� SMS-��������� -> ";
		cin >> code;

		bool found = false;

		for (size_t i{}; i < orders.GetSize(); i++)
		{
			if ((orders[i].clientPhone == number) && (code == 1111))
			{
				found = true;
				break;
			}
			else if ((orders[i].clientPhone == number) && (code != 1111)) 
			{
				while (code != 1111) 
				{
					cout << "������ �������� ���. ��������� �������\n-> ";
					cin >> code;
				}
				found = true;
				break;
			}
		}
		while (exit != 0)
		{
			if (found == true)
			{
				system("cls");

				cout << "���� ������� �������:\n\n";
				cout << "�����: " << number << "\n\n";

				cout << std::setw(19) << "Name | " << std::setw(14) << "Date | " << std::setw(16) << "Status | " << std::endl;

				for (int i{}; i < 48; ++i)
				{
					cout << "-";
				}

				cout << "\n";

				for (int i{}; i < orders.GetSize(); ++i)
				{
					if (orders[i].clientPhone == number)
					{
						for (int j{}; j < orders[i].PozitionsList.size(); ++j)
						{
							cout << std::setw(16) << orders[i].PozitionsList[j].name << " | ";

							if (orders[i].date.day < 10 && orders[i].date.month < 10)
							{
								cout << std::setw(2) << "0" << orders[i].date.day << "." << "0" << orders[i].date.month << "." << orders[i].date.year << " | ";
							}
							else if (orders[i].date.day < 10)
							{
								cout << std::setw(3) << "0" << orders[i].date.day << "." << orders[i].date.month << "." << orders[i].date.year << " | ";
							}
							else if (orders[i].date.month < 10)
							{
								cout << std::setw(3) << orders[i].date.day << "." << "0" << orders[i].date.month << "." << orders[i].date.year << " | ";
							}
							else
							{
								cout << std::setw(3) << orders[i].date.day << "." << orders[i].date.month << "." << orders[i].date.year << " | ";
							}

							if (orders[i].status == Order::Status::REGISTRARION)
							{
								cout << std::setw(13) << "Registration" << " | ";
							}
							else if (orders[i].status == Order::Status::PACKING)
							{
								cout << std::setw(13) << "Packing" << " | ";
							}
							else if (orders[i].status == Order::Status::DELIVERY)
							{
								cout << std::setw(13) << "Delivery" << " | ";
							}
							else if (orders[i].status == Order::Status::CONCLUSION)
							{
								cout << std::setw(13) << "Conclusion" << " | ";
							}

							cout << std::endl;
						}
						
					}
				}

				for (int i{}; i < 48; ++i)
				{
					cout << "-";
				}

				cout << "\n";
			}
			else
			{
				cout << "\n�� ���� ����� �������� �� ��� �������� �� ���� �����\n";
			}
			cout << "\n0. �����\n";
			if (found) { cout << "------------------------------------------------\n-> "; }
			else { cout << "----------------------------------------------------\n-> "; }
			cin >> exit;
		}
		system("cls");
	}
}