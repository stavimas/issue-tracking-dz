#include "BuyedProducts.h"
#include <iostream>
#include <iomanip>

namespace Shop
{
	void ShowBuyedProducts(OrderStorage& orders)
	{
		std::cout << "������ ���������� �������\n\n";

		std::cout << std::setw(10) << "id | " << std::setw(21) << "Name | " << std::setw(13) << "Company | "
			<< std::setw(10) << "Ram memory | " << std::setw(11) << "OS | " << std::setw(14) << "Screen diagonal | "
			<< std::setw(11) << "Cost | " << std::endl;

		for (int i{}; i < 96; ++i)
		{
			std::cout << "-";
		}

		std::cout << "\n";

		unsigned long sumCost{};

		for (int i{}; i < orders.GetSize(); ++i)
		{
			for (int j{}; j < orders[i].PozitionsList.size(); ++j)
			{
				std::cout << std::setw(7) << orders[i].PozitionsList[j].id << " | " <<
				std::setw(18) << orders[i].PozitionsList[j].name << " | " 
				<< std::setw(10) << orders[i].PozitionsList[j].company << " | " 
				<< std::setw(10) << orders[i].PozitionsList[j].ramMemory << " | "
				<< std::setw(8) << orders[i].PozitionsList[j].OS << " | "
				<< std::setw(15) << orders[i].PozitionsList[j].screenDiagonal << " | "
				<< std::setw(8) << orders[i].PozitionsList[j].cost << " | ";

				sumCost += orders[i].PozitionsList[j].cost;

				std::cout << std::endl;
			}
		}

		for (int i{}; i < 96; ++i)
		{
			std::cout << "-";
		}

		std::cout << std::endl << std::setw(10) << " | " << std::setw(18) << "�����" << " | " << std::setw(13) << " | "<< std::setw(13) << " | "<< std::setw(11) << " | "<< std::setw(18) << " | " << std::setw(8) << sumCost << " | " << std::endl;

		for (int i{}; i < 96; ++i)
		{
			std::cout << "-";
		}

		std::cout << std::endl << std::endl;
	}
}