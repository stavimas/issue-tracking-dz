#include "GetProductInput.h"

namespace Shop
{
    void GetTextInput(string m_input)
    {
        if (m_input.size() == 0)
        {
            throw InputError("\n������: ������ ����");
        }
        if (m_input.size() < 2)
        {
            throw InputError("\n������: ������������ ��������");
        }
        if (m_input.size() > 35)
        {
            throw InputError("\n������: ������� ����� ��������");
        }
    }

    void GetScreenInput(string m_input)
    {
        if (m_input.size() == 0)
        {
            throw InputError("\n������: ������ ����");
        }
        if (m_input.size() > 2)
        {
            throw InputError("\n������: ������� ����� ��������");
        }

        for (size_t i{}; i < m_input.size(); ++i)
        {
            if (!isdigit(m_input[i]))
            {
                throw InputError("\n������: �������� �������� �������� �� ������ ���������� �����");
            }
        }

        unsigned short intInput = std::stoi(m_input);
        unsigned short minimum = 11;
        unsigned short maximum = 17;

        if (intInput < minimum || intInput > maximum)
        {
            throw InputError("\n������: �������� �������� �� ����������� ����������� ���������");
        }
    }

    void GetRamInput(string m_input)
    {
        if (m_input.size() == 0)
        {
            throw InputError("\n������: ������ ����");
        }
        if (m_input.size() > 2)
        {
            throw InputError("\n������: ������� ����� ��������");
        }

        for (size_t i{}; i < m_input.size(); ++i)
        {
            if (!isdigit(m_input[i]))
            {
                throw InputError("\n������: �������� �������� �������� �� ������ ���������� �����");
            }
        }

        unsigned short intInput = std::stoi(m_input);
        unsigned short minimum = 4;
        unsigned short maximum = 32;

        if (intInput < minimum || intInput > maximum)
        {
            throw InputError("\n������: �������� �������� �� ����������� ����������� ���������");
        }
    }

    void GetCostInput(string m_input)
    {
        if (m_input.size() == 0)
        {
            throw InputError("\n������: ������ ����");
        }
        if (m_input.size() > 6)
        {
            throw InputError("\n������: ������� ����� ��������");
        }

        for (size_t i{}; i < m_input.size(); ++i)
        {
            if (!isdigit(m_input[i]))
            {
                throw InputError("\n������: �������� �������� �������� �� ������ ���������� �����");
            }
        }

        unsigned long intInput = std::stoi(m_input);
        unsigned long minimum = 10000;
        unsigned long maximum = 900000;

        if (intInput < minimum || intInput > maximum)
        {
            throw InputError("\n������: �������� �������� �� ����������� ����������� ���������e");
        }
    }

    void GetCountInput(string m_input)
    {
        if (m_input.size() == 0)
        {
            throw InputError("\n������: ������ ����");
        }
        if (m_input.size() > 3)
        {
            throw InputError("\n������: ������� ����� ��������");
        }

        for (size_t i{}; i < m_input.size(); ++i)
        {
            if (!isdigit(m_input[i]))
            {
                throw InputError("\n������: �������� �������� �������� �� ������ ���������� �����");
            }
        }

        unsigned int intInput = std::stoi(m_input);
        unsigned int minimum = 0;
        unsigned int maximum = 500;

        if (intInput < minimum || intInput > maximum)
        {
            throw InputError("\n������: �������� �������� �� ����������� ����������� ���������e");
        }
    }

    void GetIdInput(string m_input)
    {
        if (m_input.size() == 0)
        {
            throw InputError("\n������: ������ ����");
        }
        if (m_input.size() > 6)
        {
            throw InputError("\n������: ������� ����� ��������");
        }
        if (m_input.size() < 6)
        {
            throw InputError("\n������: ������������ ��������");
        }

        for (size_t i{}; i < m_input.size(); ++i)
        {
            if (!isdigit(m_input[i]))
            {
                throw InputError("\n������: �������� �������� �������� �� ������ ���������� �����");
            }
        }
    }
}