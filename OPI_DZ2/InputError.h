#ifndef INPUTERROR_H
#define INPUTERROR_H

#include <string>

using std::string;

namespace Shop
{
    class InputError {
    public:

        InputError(const char* error) : m_error(error) {}

        const char* what()
        {
            return m_error;
        }

    private:
        const char* m_error{};
    };
}

#endif //INPUTERROR_H
