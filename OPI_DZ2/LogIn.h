#ifndef LOGIN_H
#define LOGIN_H

#include "UserStorage.h";

namespace Shop
{
	unsigned long LogIn(UserStorage& users);
}

#endif // LOGIN_H