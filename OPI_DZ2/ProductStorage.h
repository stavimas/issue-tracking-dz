#ifndef PRODUCT_STORAGE_H
#define PRODUCT_STORAGE_H
#include "Product.h"
#include <string>
#include <vector>

using std::string;
using std::vector;

namespace Shop
{
	class ProductStorage
	{
	public:

		ProductStorage();
		ProductStorage(string path);

		void Write();
		void Read();

		void Add(Product product);
		void Remove(int index);

		size_t GetSize();

		Product& operator[](int index);

	private:
		string m_path{};
		vector <Product> m_productList{};
	};
}

#endif // !PRODUCT_STORAGE_H
