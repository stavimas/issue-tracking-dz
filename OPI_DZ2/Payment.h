#ifndef PAYMENT_H
#define PAYMENT_H

#include "ProductStorage.h"
#include "OrderStorage.h"
#include "ShowProductList.h"
#include <iostream>
#include <ctime>

namespace Shop
{
	void Payment(ProductStorage& products, OrderStorage& orders);
}

#endif // PAYMENT_H