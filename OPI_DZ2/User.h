#ifndef USER_H
#define USER_H

#include <string>

using std::string;

namespace Shop
{
	struct User
	{
		enum Role
		{
			CLIENT,
			ADMIN
		};

		unsigned long id{};
		Role role{};
		string fullName{};
		string login{};
		string password{};
	};
}

#endif // USER_H
