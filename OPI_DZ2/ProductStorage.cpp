#include "ProductStorage.h"
#include <fstream>

namespace Shop
{
	ProductStorage::ProductStorage()
	{
		m_path = {};
		m_productList = {};
	}

	ProductStorage::ProductStorage(string path)
	{
		m_path = path;
		Read();
	}

	void ProductStorage::Write()
	{
		std::ofstream fileWrite(m_path, std::ios::binary);

		size_t size = m_productList.size();
		fileWrite.write((char*)&size, sizeof(size_t));

		vector <Product>::const_iterator it = m_productList.begin();
		while (it != m_productList.end())
		{
			fileWrite.write((char*)&it->id, sizeof(unsigned long));
			fileWrite.write((char*)&it->cost, sizeof(unsigned long));

			fileWrite.write((char*)&it->count, sizeof(unsigned int));

			fileWrite.write((char*)&it->presence, sizeof(bool));

			fileWrite.write((char*)&it->screenDiagonal, sizeof(unsigned short));
			fileWrite.write((char*)&it->ramMemory, sizeof(unsigned short));

			size_t stringSize = it->name.length() + 1;
			fileWrite.write((char*)&stringSize, sizeof(size_t));
			fileWrite.write(it->name.c_str(), stringSize);

			stringSize = it->company.length() + 1;
			fileWrite.write((char*)&stringSize, sizeof(size_t));
			fileWrite.write(it->company.c_str(), stringSize);

			stringSize = it->OS.length() + 1;
			fileWrite.write((char*)&stringSize, sizeof(size_t));
			fileWrite.write(it->OS.c_str(), stringSize);

			++it;
		}

		fileWrite.close();
	}

	void ProductStorage::Read()
	{
		std::ifstream fileRead(m_path, std::ios::binary);

		size_t size{};
		fileRead.read((char*)&size, sizeof(size_t));

		for (size_t i{}; i < size; ++i)
		{
			Product product{};

			fileRead.read((char*)&product.id, sizeof(unsigned long));
			fileRead.read((char*)&product.cost, sizeof(unsigned long));

			fileRead.read((char*)&product.count, sizeof(unsigned int));

			fileRead.read((char*)&product.presence, sizeof(bool));

			fileRead.read((char*)&product.screenDiagonal, sizeof(unsigned short));
			fileRead.read((char*)&product.ramMemory, sizeof(unsigned short));

			size_t stringSize{};
			fileRead.read((char*)&stringSize, sizeof(size_t));
			char* tempStr = new char[stringSize] {};
			fileRead.read(tempStr, stringSize);
			product.name = tempStr;
			delete[] tempStr;

			fileRead.read((char*)&stringSize, sizeof(size_t));
			tempStr = new char[stringSize] {};
			fileRead.read(tempStr, stringSize);
			product.company = tempStr;
			delete[] tempStr;

			fileRead.read((char*)&stringSize, sizeof(size_t));
			tempStr = new char[stringSize] {};
			fileRead.read(tempStr, stringSize);
			product.OS = tempStr;
			delete[] tempStr;

			m_productList.push_back(product);
		}

		fileRead.close();
	}

	void ProductStorage::Add(Product product)
	{
		m_productList.push_back(product);
		Write();
	}

	void ProductStorage::Remove(int index)
	{
		m_productList.erase(m_productList.begin() + index);
		Write();
	}

	size_t ProductStorage::GetSize() { return m_productList.size(); }

	Product& ProductStorage::operator[](int index) { return m_productList[index]; }
}