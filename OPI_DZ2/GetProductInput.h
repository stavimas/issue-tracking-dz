#ifndef GETPRODUCTINPUT_H
#define GETPRODUCTINPUT_H

#include "InputError.h"

namespace Shop
{
    void GetTextInput(string m_input);
    void GetScreenInput(string m_input);
    void GetRamInput(string m_input);
    void GetCostInput(string m_input);
    void GetCountInput(string m_input);
    void GetIdInput(string m_input);
}

#endif //GETPRODUCTINPUT_H
