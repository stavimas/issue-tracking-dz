#include "Payment.h"
#include "InputError.h"
#include <iomanip>
#include <string>
#include <ctime>

namespace Shop
{
	void Payment(ProductStorage& products, OrderStorage& orders)
	{
		srand(time(NULL));
		unsigned long buyId{};
		bool exit = true;
		bool successfulPurchase = false;

		Order order{};

		order.id = 100000 + rand() % 900000;
		
		std::cout << "��� ���������� ������� ������� ����� �������� -> ";
		string number;
		std::cin >> number;

		bool flag = false;
		while (flag == false)
		{
			try
			{
				if (number.size() > 11)
				{
					throw InputError("\n������: ����� �� ������ ��������� ����� 11 ����\n\n");
				}
				if (number.size() < 11)
				{
					throw InputError("\n������: ����� �� ������ ��������� ����� 11 ����\n\n");
				}
				if (number[0] != '8')
				{
					throw InputError("\n������: ����� ������ ���������� � '8'\n\n");
				}

				else { flag = true; }
			}

			catch (InputError& exception)
			{
				std::cerr << exception.what() <<"-> ";
				std::cin >> number;
			}
		}

		order.clientPhone = number;

		while (exit)
		{
			system("cls");

			ShowProductList(products);

			std::cout << '\n';
			std::cout << "������� id ������, ������� �� ������ ������ -> ";
			std::cin >> buyId;

			bool flag = false;

			while (flag == false)
			{
				int i{};

				for (int j{}; j < products.GetSize(); ++j)
				{
					if (products[j].id != buyId) { i++; }
					else { flag = true; }
					
				}
				if (i == products.GetSize())
				{
					std::cout << "\n�� ����� �������� id ������!\n\n������� ������ id ������, ������� �� ������ ������ -> ";
					std::cin >> buyId;
				}
			}
			for (int j{}; j < products.GetSize(); ++j)
			{
				if (products[j].id == buyId && products[j].presence == true)
				{
					flag = true;
					order.PozitionsList.push_back(products[j]);
					std::cout << "\n����� " << buyId << " ������� �������� � ������ �������!" << std::endl << std::endl;
					successfulPurchase = true;
					std::cout << "������� 1, ���� ������ ���������� �������, � 0, ���� ������ ����� �� ������� ������� -> ";
					std::cin >> exit;
				}
				else if (products[j].id == buyId && products[j].presence == false)
				{
					flag = true;
					std::cout << "\n" << buyId << " ��� � �������!" << std::endl;
					std::cout << "\n������� 1, ���� ������ ���������� �������, � 0, ���� ������ ����� �� ������� ������� -> ";
					std::cin >> exit;
				}
			}
		}
		if (exit == 0) { system("cls"); }

		if (successfulPurchase)
		{
			std::time_t rawtime = std::time(0);
			struct tm now;
			localtime_s(&now, &rawtime);
			order.date.day = now.tm_mday;
			order.date.month = now.tm_mon + 1;
			order.date.year = now.tm_year + 1900;

			order.status = Order::Status::REGISTRARION;

			orders.Add(order);
		}
		
	}
}