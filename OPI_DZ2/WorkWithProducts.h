#ifndef WORK_WITH_PRODUCTS_H
#define WORK_WITH_PRODUCTS_H

#include "ProductStorage.h"

namespace Shop
{
	void SortProduct(ProductStorage& products);
	void FilterProduct(ProductStorage& products);
}

#endif // WORK_WITH_PRODUCTS_H
