#include "ComputerEdit.h"
#include "GetProductInput.h"
#include <iostream>
#include <string>
#include <ctime>

using std::cin;
using std::cout;

namespace Shop
{
	void AddProductList(ProductStorage& productStorage)
	{
		srand(time(NULL));
		cout << "���������� ������ ������\n";
		cout << "------------------------\n";
		
		Product newProd{};
		newProd.id = 100000 + rand() % 900000;

		cin.ignore();
		cout << "������������: ";
		getline(cin, newProd.name);

		try { GetTextInput(newProd.name); }
		catch (InputError& exception)
		{
			std::cerr << exception.what() << "\n";
			cin.ignore();
			system("cls");
			return;
		}

		cout << "����: ";
		string cost{};
		getline(cin, cost);

		try { GetCostInput(cost); }
		catch (InputError& exception)
		{
			std::cerr << exception.what() << "\n";
			cin.ignore();
			system("cls");
			return;
		}
		newProd.cost = std::stoi(cost);

		cout << "����������: ";
		string count{};
		getline(cin, count);
		try { GetCountInput(count); }
		catch (InputError& exception)
		{
			std::cerr << exception.what() << "\n";
			cin.ignore();
			system("cls");
			return;
		}
		newProd.count = std::stoi(count);


		if (newProd.count >= 1)
		{
			newProd.presence = 1;
		}
		else
		{
			newProd.presence = 0;
		}

		cout << "��������-�������������: ";
		getline(cin, newProd.company);

		try { GetTextInput(newProd.company); }
		catch (InputError& exception)
		{
			std::cerr << exception.what() << "\n";
			cin.ignore();
			system("cls");
			return;
		}

		cout << "��������� ������: ";
		string screenDiagonal{};
		getline(cin, screenDiagonal);
		try { GetScreenInput(screenDiagonal); }
		catch (InputError& exception)
		{
			std::cerr << exception.what() << "\n";
			cin.ignore();
			system("cls");
			return;
		}
		newProd.screenDiagonal = std::stoi(screenDiagonal);

		cout << "����������� ������: ";
		string ram{};
		getline(cin, ram);
		try { GetRamInput(ram); }
		catch (InputError& exception)
		{
			std::cerr << exception.what() << "\n";
			cin.ignore();
			system("cls");
			return;
		}
		newProd.ramMemory = std::stoi(ram);

		cout << "������������ �������: ";
		cin >> newProd.OS;

		try { GetTextInput(newProd.OS); }
		catch (InputError& exception)
		{
			std::cerr << exception.what() << "\n";
			cin.ignore();
			system("cls");
			return;
		}

		productStorage.Add(newProd);
	}

	void EditProductList(ProductStorage& productStorage)
	{
		string input{};
		unsigned long editId{};
		cout << "\n������� ID ������, ������� ������ ��������: ";
		cin.ignore();
		getline(cin, input);

		bool flag = false;
		while (flag == false)
		{
			try
			{
				if (input.size() == 0)
				{
					throw InputError("\n������: ������ ����\n\n");
				}
				else if (input.size() > 6)
				{
					throw InputError("\n������: ������� ����� ��������\n\n");
				}
				else if (input.size() < 6)
				{
					throw InputError("\n������: ������������ ��������\n\n");
				}
				else { flag = true; }

				for (size_t i{}; i < input.size(); ++i)
				{
					flag = false;
					if (!isdigit(input[i]))
					{
						throw InputError("\n������: ���� �������� �� ������ ���������� �����\n\n");
					}
					else { flag = true; }
				}
			}

			catch (InputError& exception)
			{
				std::cerr << exception.what() << "-> ";
				getline(cin, input);
			}
		}

		editId = std::stoi(input);
		cout << std::endl;

		for (int i = 0; i < productStorage.GetSize(); ++i)
		{
			if (productStorage[i].id == editId)
			{
				cout << "������� << - >>, ���� �� ������ ������ ����\n\n";
				string newName;
				cout << "������������: " << productStorage[i].name << " -> ";
				getline(cin, newName);
				if (newName != "-")
				{
					try { GetTextInput(newName); }
					catch (InputError& exception)
					{
						std::cerr << exception.what() << "\n";
						cin.ignore();
						system("cls");
						return;
					}
					productStorage[i].name = newName;
				}
				
				string newCost;
				cout << "����: " << productStorage[i].cost << " -> ";
				getline(cin, newCost);

				if (newCost != "-")
				{
					try { GetCostInput(newCost); }
					catch (InputError& exception)
					{
						std::cerr << exception.what() << "\n";
						cin.ignore();
						system("cls");
						return;
					}
					productStorage[i].cost = std::stoi(newCost);
				}
				
				string newCount;
				cout << "����������: " << productStorage[i].count << " -> ";
				getline(cin, newCount);

				if (newCount != "-")
				{
					try { GetCountInput(newCount); }
					catch (InputError& exception)
					{
						std::cerr << exception.what() << "\n";
						cin.ignore();
						system("cls");
						return;
					}
					productStorage[i].count = std::stoi(newCount);
				}

				if (productStorage[i].count == 0) 
				{
					productStorage[i].presence = 0;
				}
				else
				{
					productStorage[i].presence = 1;
				}

				string newCompany;
				cout << "��������-�������������: " << productStorage[i].company << " -> ";
				getline(cin, newCompany);
				if (newCompany != "-")
				{
					try { GetTextInput(newCompany); }
					catch (InputError& exception)
					{
						std::cerr << exception.what() << "\n";
						cin.ignore();
						system("cls");
						return;
					}
					productStorage[i].company = newCompany;
				}

				string newScreenDiagonal;
				cout << "��������� �����a: " << productStorage[i].screenDiagonal << " -> ";
				getline(cin, newScreenDiagonal);
				if (newScreenDiagonal != "-")
				{
					try { GetScreenInput(newScreenDiagonal); }
					catch (InputError& exception)
					{
						std::cerr << exception.what() << "\n";
						cin.ignore();
						system("cls");
						return;
					}
					productStorage[i].screenDiagonal = std::stoi(newScreenDiagonal);
				}

				string newRamMemory;
				cout << "����������� ������: " << productStorage[i].ramMemory << " -> ";
				getline(cin, newRamMemory);
				if (newRamMemory != "-")
				{
					try { GetRamInput(newRamMemory); }
					catch (InputError& exception)
					{
						std::cerr << exception.what() << "\n";
						cin.ignore();
						system("cls");
						return;
					}
					productStorage[i].ramMemory = std::stoi(newRamMemory);
				}

				string newOS;
				cout << "������������ �������: " << productStorage[i].OS << " -> ";
				getline(cin, newOS);
				if (newOS != "-")
				{
					try { GetTextInput(newOS); }
					catch (InputError& exception)
					{
						std::cerr << exception.what() << "\n";
						cin.ignore();
						system("cls");
						return;
					}
					productStorage[i].OS = newOS;
				}
				break;
			}
		}
		system("cls");
		productStorage.Write();
	}

}