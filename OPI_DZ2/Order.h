#ifndef ORDER_H
#define ORDER_H

#include "Product.h"
#include <string>
#include <vector>

using std::vector;
using std::string;

namespace Shop
{
	struct Order
	{
		enum Status
		{
			REGISTRARION,
			PACKING,
			DELIVERY,
			CONCLUSION
		};

		struct Date
		{
			unsigned short day;
			unsigned short month;
			unsigned short year;
		};

		unsigned long id{};
		string clientPhone{};
		Date date{};
		Status status{};
		vector<Product> PozitionsList{};
	};
}

#endif // ORDER_H
