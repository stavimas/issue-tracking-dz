#ifndef SHOW_ORDER_LIST_H
#define SHOW_ORDER_LIST_H

#include "OrderStorage.h"
#include "EditOrderStatus.h"

namespace Shop
{
	void ShowOrderList(OrderStorage& orders);
}

#endif // SHOW_ORDER_LIST_H
