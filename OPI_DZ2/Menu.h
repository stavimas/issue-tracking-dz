#ifndef MENU_H
#define MENU_H

#include "ComputerEdit.h"
#include "WorkWithProducts.h"
#include "ShowProductList.h"
#include "LogIn.h"
#include "Payment.h"
#include "ShowOrderList.h"
#include "BuyedProducts.h"
#include "SearchProduct.h"
#include "OrderHistory.h"

namespace Shop
{
	class Menu
	{
	public:
		Menu(OrderStorage& orderStorage, UserStorage& userStorage, ProductStorage& productStorage);

		void Draw();

	private:
		unsigned long m_authorizedId{};
		User::Role m_role{};
		OrderStorage m_orderStorage{};
		UserStorage m_userStorage{};
		ProductStorage m_productStorage{};
	};
}

#endif // !MENU_H