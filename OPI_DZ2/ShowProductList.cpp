#include "ShowProductList.h"
#include <iostream>
#include <iomanip>

namespace Shop
{
	void ShowProductList(ProductStorage& products)
	{
		std::cout << std::setw(10) << "id | " << std::setw(21) << "Name | " << std::setw(16) << "Company | "
			<< std::setw(13) << "Ram memory | " << std::setw(11) << "OS | " << std::setw(17) << "Screen diagonal | "
			<< std::setw(9) << "Cost | " << std::setw(8) << "Count | " << std::setw(10) << "Presence |" << std::endl;

		for (int i{}; i < 116; ++i)
		{
			std::cout << "-";
		}

		std::cout << std::endl;

		for (int i{}; i < products.GetSize(); ++i)
		{
			std::cout << std::setw(7) << products[i].id << " | " << std::setw(18) <<  products[i].name << " | "
				<< std::setw(13) << products[i].company << " | " << std::setw(10) << products[i].ramMemory << " | "
				<< std::setw(8) << products[i].OS << " | " << std::setw(15) << products[i].screenDiagonal << " | "
				<< std::setw(6) << products[i].cost << " | " << std::setw(5) << products[i].count << " | ";

			if (products[i].presence)
			{
				std::cout << std::setw(10) << "�� |";
			}
			else
			{
				std::cout << std::setw(10) <<  "��� |";
			}

			std::cout << std::endl;
		}

		for (int i{}; i < 116; ++i)
		{
			std::cout << "-";
		}

		std::cout << std::endl;
	}
}