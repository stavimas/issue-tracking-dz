#ifndef SHOW_PRODUCT_LIST_H
#define SHOW_PRODUCT_LIST_H

#include "ProductStorage.h"

namespace Shop
{
	void ShowProductList(ProductStorage& products);
}

#endif // SHOW_PRODUCT_LIST_H
