#include "EditOrderStatus.h"
#include "Order.h"
#include <iostream>

namespace Shop
{
	void EditOrderStatus(OrderStorage& orders)
	{
		bool found = false;
		unsigned long orderId{};
		Order::Status orderStatus{};

		std::cout << "\n������� id ������ -> ";
		std::cin >> orderId;

		for (size_t i{}; i < orders.GetSize(); i++)
		{
			if (orders[i].id == orderId) 
			{ 
				orderStatus = orders[i].status;
				found = true; 
				break;
			}
		}

		if (found)
		{
			std::cout << "\n������ ������: ";
			if (orderStatus == Order::Status::REGISTRARION)
			{
				std::cout << "Registration\n";
			}
			else if (orderStatus == Order::Status::PACKING)
			{
				std::cout << "Packing\n";
			}
			else if (orderStatus == Order::Status::DELIVERY)
			{
				std::cout << "Delivery\n";
			}
			else if (orderStatus == Order::Status::CONCLUSION)
			{
				std::cout << "Conclusion\n";
			}

			std::cout << "\n������� ����� ������ ������: \n";
			std::cout << "0 - Registration\n";
			std::cout << "1 - Packing\n";
			std::cout << "2 - Delivery\n";
			std::cout << "3 - Conclusion\n";
			std::cout << "-> ";

			int temp{};
			std::cin >> temp;

			if (temp == 0)
			{
				orderStatus = Order::Status::REGISTRARION;
			}
			else if (temp == 1)
			{
				orderStatus = Order::Status::PACKING;
			}
			else if (temp == 2)
			{
				orderStatus = Order::Status::DELIVERY;
			}
			else if (temp == 3)
			{
				orderStatus = Order::Status::CONCLUSION;
			}
			else
			{
				std::cout << "\n�� ����� ������������ ������ ������!\n";
			}

			for (size_t i{}; i < orders.GetSize(); i++)
			{
				if (orders[i].id == orderId)
				{
					orders[i].status = orderStatus;;
					break;
				}
			}

			orders.Write();
		}
		else
		{
			std::cout << "\n�������� id �� ������!" << std::endl;
			std::cin.ignore();
			std::cin.ignore();
			system("cls");
		}
	}
}