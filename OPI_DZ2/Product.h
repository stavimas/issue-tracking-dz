#ifndef PRODUCT_H
#define PRODUCT_H

#include <string>

using std::string;

namespace Shop
{
	struct Product
	{
		unsigned long id{};
		unsigned long cost{};
		unsigned int count{};
		bool presence{};
		unsigned short screenDiagonal{};
		unsigned short ramMemory{};
		string name{};
		string company{};
		string OS{};
	};
}

#endif // PRODUCT_H
