#ifndef COMPUTEREDIT_H
#define COMPUTEREDIT_H

#include "ProductStorage.h"

namespace Shop
{
	void AddProductList(ProductStorage& productStorage);
	void EditProductList(ProductStorage& productStorage);
}

#endif // COMPUTEREDIT_H
