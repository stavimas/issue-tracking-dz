#include "ShowOrderList.h"
#include <iostream>
#include <iomanip>

namespace Shop
{
	void ShowOrderList(OrderStorage& orders)
	{
		unsigned short command = 1;
		system("cls");
		while (command != 0)
		{
			std::cout << "������ �������\n\n";

			std::cout << std::setw(10) << "id | " << std::setw(14) << "Client Phone | " << std::setw(14) << "Date | "
				<< std::setw(16) << "Status | " << std::endl;

			for (int i{}; i < 53; ++i)
			{
				std::cout << "-";
			}

			std::cout << "\n";

			for (int i{}; i < orders.GetSize(); ++i)
			{
				std::cout << std::setw(7) << orders[i].id << " | " << std::setw(12) << orders[i].clientPhone << " | ";

				if (orders[i].date.day < 10 && orders[i].date.month < 10) 
				{ 
					std::cout << std::setw(2) << "0" << orders[i].date.day << "." << "0" << orders[i].date.month << "." << orders[i].date.year << " | "; 
				}
				else if (orders[i].date.day < 10)
				{
					std::cout << std::setw(3) << "0" << orders[i].date.day << "." << orders[i].date.month << "." << orders[i].date.year << " | ";
				}
				else if (orders[i].date.month < 10)
				{
					std::cout << std::setw(3) << orders[i].date.day << "." << "0" << orders[i].date.month << "." << orders[i].date.year << " | ";
				}
				else
				{
					std::cout << std::setw(3) << orders[i].date.day << "." << orders[i].date.month << "." << orders[i].date.year << " | ";
				}

				if (orders[i].status == Order::Status::REGISTRARION)
				{
					std::cout << std::setw(13) << "Registration" << " | ";
				}
				else if (orders[i].status == Order::Status::PACKING)
				{
					std::cout << std::setw(13) << "Packing" << " | ";
				}
				else if (orders[i].status == Order::Status::DELIVERY)
				{
					std::cout << std::setw(13) << "Delivery" << " | ";
				}
				else if (orders[i].status == Order::Status::CONCLUSION)
				{
					std::cout << std::setw(13) << "Conclusion" << " | ";
				}

				std::cout << std::endl;
			}

			for (int i{}; i < 53; ++i)
			{
				std::cout << "-";
			}

			std::cout << "\n\n";

			std::cout << "1. �������� ������ ������\n\n";
			std::cout << "0. �����\n";
			std::cout << "-------------------------\n";
			std::cout << "-> ";

			std::cin >> command;

			if (command == 1) { EditOrderStatus(orders); }
			if (command == 0) { system("cls"); }
		}
	}
}