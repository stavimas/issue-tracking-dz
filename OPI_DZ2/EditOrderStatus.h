#ifndef EDIT_ORDER_STATUS_H
#define EDIT_ORDER_STATUS_H

#include "OrderStorage.h"

namespace Shop
{
	void EditOrderStatus(OrderStorage& orders);
}

#endif // !EDIT_ORDER_STATUS_H
