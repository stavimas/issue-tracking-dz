#include <iostream>
#include "LogIn.h"
#include "GetLoginInput.h"

namespace Shop
{
	unsigned long LogIn(UserStorage& users)
	{
		system("cls");
		bool auth = false;
		User checkUser;

		while (auth == false)
		{
			std::cin.ignore();
			std::cout << "������� ������ ��� �����������:\n";
			std::cout << "-------------------------------\n";
			std::cout << "������� ���� ����� -> ";
			std::getline(std::cin, checkUser.login);

			bool flag = false;
			while (flag == false)
			{
				try
				{
					if (checkUser.login.size() == 0)
					{
						throw InputError("\n������: ������ ����\n\n");
					}
					else if (checkUser.login.size() < 5)
					{
						throw InputError("\n������: ������������ ��������\n\n");
					}
					else if (checkUser.login.size() > 20)
					{
						throw InputError("\n������: ������� ����� ��������\n\n");
					}
					else { flag = true; }
				}

				catch (InputError& exception)
				{
					std::cerr << exception.what() << "-> ";
					std::getline(std::cin, checkUser.login);
					std::cout << "\n";
				}
			}

			std::cout << "������� ���� ������ -> ";
			std::getline(std::cin, checkUser.password);

			flag = false;
			while (flag == false)
			{
				try
				{
					if (checkUser.password.size() == 0)
					{
						throw InputError("\n������: ������ ����\n\n");
					}
					else if (checkUser.password.size() < 5)
					{
						throw InputError("\n������: ������������ ��������\n\n");
					}
					else if (checkUser.password.size() > 20)
					{
						throw InputError("\n������: ������� ����� ��������\n\n");
					}
					else { flag = true; }
				}

				catch (InputError& exception)
				{
					std::cerr << exception.what() << "-> ";
					std::getline(std::cin, checkUser.password);
					std::cout << "\n";
				}
			}

			for (size_t i{}; i < users.GetSize(); ++i)
			{
				if (checkUser.login == users[i].login && checkUser.password == users[i].password)
				{
					auth = true;
					return users[i].id;
				}
			}

			system("cls");
			std::cout << "�� ����� �������� ����� ��� ������!\n\n";
		}
	}
}