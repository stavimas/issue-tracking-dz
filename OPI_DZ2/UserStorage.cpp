#include "UserStorage.h"
#include <fstream>

namespace Shop
{
	UserStorage::UserStorage()
	{
		m_path = {};
		m_userList = {};
	}

	UserStorage::UserStorage(string path)
	{
		m_path = path;
		Read();
	}

	void UserStorage::Write()
	{
		std::ofstream fileWrite(m_path, std::ios::binary);

		size_t size = m_userList.size();
		fileWrite.write((char*)&size, sizeof(size_t));

		vector <User>::const_iterator it = m_userList.begin();
		while (it != m_userList.end())
		{
			fileWrite.write((char*)&it->id, sizeof(unsigned long));
			fileWrite.write((char*)&it->role, sizeof(User::Role));

			size_t stringSize = it->fullName.length() + 1;
			fileWrite.write((char*)&stringSize, sizeof(size_t));
			fileWrite.write(it->fullName.c_str(), stringSize);

			stringSize = it->login.length() + 1;
			fileWrite.write((char*)&stringSize, sizeof(size_t));
			fileWrite.write(it->login.c_str(), stringSize);

			stringSize = it->password.length() + 1;
			fileWrite.write((char*)&stringSize, sizeof(size_t));
			fileWrite.write(it->password.c_str(), stringSize);

			++it;
		}

		fileWrite.close();
	}

	void UserStorage::Read()
	{
		std::ifstream fileRead(m_path, std::ios::binary);

		size_t size{};
		fileRead.read((char*)&size, sizeof(size_t));

		for (size_t i{}; i < size; ++i)
		{
			User user{};

			fileRead.read((char*)&user.id, sizeof(unsigned long));
			fileRead.read((char*)&user.role, sizeof(User::Role));

			size_t stringSize{};
			fileRead.read((char*)&stringSize, sizeof(size_t));
			char* tempStr = new char[stringSize] {};
			fileRead.read(tempStr, stringSize);
			user.fullName = tempStr;
			delete[] tempStr;

			fileRead.read((char*)&stringSize, sizeof(size_t));
			tempStr = new char[stringSize] {};
			fileRead.read(tempStr, stringSize);
			user.login = tempStr;
			delete[] tempStr;

			fileRead.read((char*)&stringSize, sizeof(size_t));
			tempStr = new char[stringSize] {};
			fileRead.read(tempStr, stringSize);
			user.password = tempStr;
			delete[] tempStr;

			m_userList.push_back(user);
		}

		fileRead.close();
	}

	void UserStorage::Add(User user)
	{
		m_userList.push_back(user);
		Write();
	}

	void UserStorage::Remove(int index)
	{
		m_userList.erase(m_userList.begin() + index);
		Write();
	}

	size_t UserStorage::GetSize() { return m_userList.size(); }

	User& UserStorage::operator[](int index) { return m_userList[index]; }
}