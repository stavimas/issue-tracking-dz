#include "WorkWithProducts.h"
#include "ShowProductList.h"
#include "InputError.h"
#include <vector>
#include <iostream>
#include <iomanip>

using std::cout;
using std::cin;

namespace Shop
{
	void SortProduct(ProductStorage& products)
	{
		system("cls");

		ProductStorage productSort = products;

		unsigned int sortCom = 1;

		while (sortCom != 0)
		{
			ShowProductList(productSort);

			cout << "\n�������������:\n";
			cout << "	1. �� ������������ ��������� �������\n";
			cout << "	2. �� �������� ��������� �������\n\n";
			cout << "	3. �� ����������� ���������� �������\n";
			cout << "	4. �� �������� ���������� �������\n\n";
			cout << "	5. �� ����������� �������� ��������� ������\n";
			cout << "	6. �� �������� �������� ��������� ������\n\n";
			cout << "	7. �� ����������� �������� ����������� ������\n";
			cout << "	8. �� �������� �������� ����������� ������\n\n";
			cout << "	0. �����\n";
			cout << "-----------------------------------------------------\n-> ";

			cin >> sortCom;

			bool flag = false;
			while (flag == false)
			{
				try
				{
					if (sortCom > 8)
					{
						throw InputError("\n������: ������������ ����\n\n");
					}

					else { flag = true; }
				}

				catch (InputError& exception)
				{
					std::cerr << exception.what() << "-> ";
					std::cin >> sortCom;
				}
			}

			if (sortCom == 1)
			{
				for (int i{}; i < productSort.GetSize(); i++) {
					for (int j{}; j < productSort.GetSize() - i - 1; j++) {
						if (productSort[j].cost > productSort[j + 1].cost) {
							Product temp = productSort[j];
							productSort[j] = productSort[j + 1];
							productSort[j + 1] = temp;
						}
					}
				}
				system("cls");
				cout << "���������� �� ����������� ��������� ������� ������ �������!\n\n";
			}
			if (sortCom == 2)
			{
				for (int i{}; i < productSort.GetSize(); i++) {
					for (int j{}; j < productSort.GetSize() - i - 1; j++) {
						if (productSort[j].cost < productSort[j + 1].cost) {
							Product temp = productSort[j];
							productSort[j] = productSort[j + 1];
							productSort[j + 1] = temp;
						}
					}
				}
				system("cls");
				cout << "���������� �� �������� ��������� ������� ������ �������!\n\n";
			}
			if (sortCom == 3)
			{
				for (int i{}; i < productSort.GetSize(); i++) {
					for (int j{}; j < productSort.GetSize() - i - 1; j++) {
						if (productSort[j].count > productSort[j + 1].count) {
							Product temp = productSort[j];
							productSort[j] = productSort[j + 1];
							productSort[j + 1] = temp;
						}
					}
				}
				system("cls");
				cout << "���������� �� ����������� ���������� ������� ������ �������!\n\n";
			}
			if (sortCom == 4)
			{
				for (int i{}; i < productSort.GetSize(); i++) {
					for (int j{}; j < productSort.GetSize() - i - 1; j++) {
						if (productSort[j].count < productSort[j + 1].count) {
							Product temp = productSort[j];
							productSort[j] = productSort[j + 1];
							productSort[j + 1] = temp;
						}
					}
				}
				system("cls");
				cout << "���������� �� �������� ���������� ������� ������ �������!\n\n";
			}
			if (sortCom == 5)
			{
				for (int i{}; i < productSort.GetSize(); i++) {
					for (int j{}; j < productSort.GetSize() - i - 1; j++) {
						if (productSort[j].screenDiagonal > productSort[j + 1].screenDiagonal) {
							Product temp = productSort[j];
							productSort[j] = productSort[j + 1];
							productSort[j + 1] = temp;
						}
					}
				}
				system("cls");
				cout << "���������� �� ����������� �������� ��������� ������ ������ �������!\n\n";
			}
			if (sortCom == 6)
			{
				for (int i{}; i < productSort.GetSize(); i++) {
					for (int j{}; j < productSort.GetSize() - i - 1; j++) {
						if (productSort[j].screenDiagonal < productSort[j + 1].screenDiagonal) {
							Product temp = productSort[j];
							productSort[j] = productSort[j + 1];
							productSort[j + 1] = temp;
						}
					}
				}
				system("cls");
				cout << "���������� �� �������� �������� ��������� ������ ������ �������!\n\n";
			}
			if (sortCom == 7)
			{
				for (int i{}; i < productSort.GetSize(); i++) {
					for (int j{}; j < productSort.GetSize() - i - 1; j++) {
						if (productSort[j].ramMemory > productSort[j + 1].ramMemory) {
							Product temp = productSort[j];
							productSort[j] = productSort[j + 1];
							productSort[j + 1] = temp;
						}
					}
				}
				system("cls");
				cout << "���������� �� ����������� �������� ����������� ������ ������ �������!\n\n";
			}
			if (sortCom == 8)
			{
				for (int i{}; i < productSort.GetSize(); i++) {
					for (int j{}; j < productSort.GetSize() - i - 1; j++) {
						if (productSort[j].ramMemory < productSort[j + 1].ramMemory) {
							Product temp = productSort[j];
							productSort[j] = productSort[j + 1];
							productSort[j + 1] = temp;
						}
					}
				}
				system("cls");
				cout << "���������� �� �������� �������� ����������� ������ ������ �������!\n\n";
			}
		}

		if (sortCom == 0)
		{
			system("cls");
		}
	}

	void FilterProduct(ProductStorage& products)
	{
		system("cls");

		ShowProductList(products);

		unsigned int filterCom = 1;

		while (filterCom != 0)
		{
			cout << "\n�������������:\n";
			cout << "	1. �� ������������ �������: MacOS\n";
			cout << "	2. �� ������������ �������: Windows\n\n";
			cout << "	3. �� �������� ��������� ������: 13\n";
			cout << "	4. �� �������� ��������� ������: 15\n";
			cout << "	5. �� �������� ��������� ������: 17\n\n";
			cout << "	6. �� ���������� �������: < 75\n";
			cout << "	7. �� ���������� �������: > 75\n\n";
			cout << "	8. �� ��������� �������: < 50000\n";
			cout << "	9. �� ��������� �������: > 50000\n\n";
			cout << "	0. �����\n";
			cout << "-------------------------------------------\n-> ";

			cin >> filterCom;

			bool flag = false;
			while (flag == false)
			{
				try
				{
					if (filterCom > 9)
					{
						throw InputError("\n������: ������������ ����\n\n");
					}

					else { flag = true; }
				}

				catch (InputError& exception)
				{
					std::cerr << exception.what() << "-> ";
					std::cin >> filterCom;
				}
			}

			if (filterCom == 1)
			{
				ProductStorage productFilter{};
				int j{};
				for (int i{}; i < products.GetSize(); i++) {
					if (products[i].OS == "MacOS")
					{
						productFilter.Add(products[i]);
					}
				}
				system("cls");
				cout << "���������� �� ������������ �������: MacOS ������ �������!\n\n";
				ShowProductList(productFilter);
			}

			if (filterCom == 2)
			{
				ProductStorage productFilter{};
				int j{};
				for (int i{}; i < products.GetSize(); i++) {
					if (products[i].OS == "Windows")
					{
						productFilter.Add(products[i]);
					}
				}
				system("cls");
				cout << "���������� �� ������������ �������: Windows ������ �������!\n\n";
				ShowProductList(productFilter);
			}

			if (filterCom == 3)
			{
				ProductStorage productFilter{};
				int j{};
				for (int i{}; i < products.GetSize(); i++) {
					if (products[i].screenDiagonal == 13)
					{
						productFilter.Add(products[i]);
					}
				}
				system("cls");
				cout << "���������� �� �������� ��������� ������: 13 ������ �������!\n\n";
				ShowProductList(productFilter);
			}

			if (filterCom == 4)
			{
				ProductStorage productFilter{};
				int j{};
				for (int i{}; i < products.GetSize(); i++) {
					if (products[i].screenDiagonal == 15)
					{
						productFilter.Add(products[i]);
					}
				}
				system("cls");
				cout << "���������� �� �������� ��������� ������: 15 ������ �������!\n\n";
				ShowProductList(productFilter);
			}

			if (filterCom == 5)
			{
				ProductStorage productFilter{};
				int j{};
				for (int i{}; i < products.GetSize(); i++) {
					if (products[i].screenDiagonal == 17)
					{
						productFilter.Add(products[i]);
					}
				}
				system("cls");
				cout << "���������� �� �������� ��������� ������: 17 ������ �������!\n\n";
				ShowProductList(productFilter);
			}

			if (filterCom == 6)
			{
				ProductStorage productFilter{};
				int j{};
				for (int i{}; i < products.GetSize(); i++) {
					if (products[i].count < 75)
					{
						productFilter.Add(products[i]);
					}
				}
				system("cls");
				cout << "���������� �� ���������� �������: < 75 ������ �������!\n\n";
				ShowProductList(productFilter);
			}

			if (filterCom == 7)
			{
				ProductStorage productFilter{};
				int j{};
				for (int i{}; i < products.GetSize(); i++) {
					if (products[i].count > 75)
					{
						productFilter.Add(products[i]);
					}
				}
				system("cls");
				cout << "���������� �� ���������� �������: > 75 ������ �������!\n\n";
				ShowProductList(productFilter);
			}

			if (filterCom == 8)
			{
				ProductStorage productFilter{};
				int j{};
				for (int i{}; i < products.GetSize(); i++) {
					if (products[i].cost < 50000)
					{
						productFilter.Add(products[i]);
					}
				}
				system("cls");
				cout << "���������� �� ��������� �������: < 50000 ������ �������!\n\n";
				ShowProductList(productFilter);
			}

			if (filterCom == 9)
			{
				ProductStorage productFilter{};
				int j{};
				for (int i{}; i < products.GetSize(); i++) {
					if (products[i].cost > 50000)
					{
						productFilter.Add(products[i]);
					}
				}
				system("cls");
				cout << "���������� �� ��������� �������: > 50000 ������ �������!\n\n";
				ShowProductList(productFilter);
			}

			if (filterCom == 0)
			{
				system("cls");
			}
		}
	}
}