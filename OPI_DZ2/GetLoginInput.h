#ifndef GETLOGININPUT_H
#define GETLOGININPUT_H

#include "InputError.h"

namespace Shop
{
    void GetLoginInput(string m_input);
}

#endif //GETLOGININPUT_H
