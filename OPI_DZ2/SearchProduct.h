#ifndef SEARCH_PRODUCT_H
#define SEARCH_PRODUCT_H

#include "ProductStorage.h"

namespace Shop
{
	void SearchProduct(ProductStorage& products);
}

#endif // !SEARCH_PRODUCT_H
