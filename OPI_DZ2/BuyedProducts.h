#ifndef BUYED_PRODUCTS_H
#define BUYED_PRODUCTS_H

#include "OrderStorage.h"

namespace Shop
{
	void ShowBuyedProducts(OrderStorage& orders);
}

#endif // BUYED_PRODUCTS_H
