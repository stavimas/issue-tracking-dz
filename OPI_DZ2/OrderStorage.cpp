#include "OrderStorage.h"
#include "fstream";

using std::fstream;
using std::ofstream;
using std::ifstream;

namespace Shop {

	OrderStorage::OrderStorage()
	{
		m_path = {};
		m_orderList = {};
	}

	OrderStorage::OrderStorage(string path)
	{
		m_path = path;
		Read();
	}

	void OrderStorage::Write()
	{
		ofstream fileWrite(m_path, std::ios::binary);

		size_t size = m_orderList.size();
		fileWrite.write((char*)&size, sizeof(size_t));

		vector <Order>::const_iterator it = m_orderList.begin();
		
		while (it != m_orderList.end())
		{
			fileWrite.write((char*)&it->id, sizeof(unsigned long));

			size_t stringSize = it->clientPhone.length() + 1;
			fileWrite.write((char*)&stringSize, sizeof(size_t));
			fileWrite.write(it->clientPhone.c_str(), stringSize);

			fileWrite.write((char*)&it->date, sizeof(Order::Date));

			fileWrite.write((char*)&it->status, sizeof(Order::Status));

			size_t sizePozition = it->PozitionsList.size();
			fileWrite.write((char*)&sizePozition, sizeof(size_t));

			vector <Product>::const_iterator itPozition = it->PozitionsList.begin();
			while (itPozition != it->PozitionsList.end())
			{
				fileWrite.write((char*)&itPozition->id, sizeof(unsigned long));
				fileWrite.write((char*)&itPozition->cost, sizeof(unsigned long));

				fileWrite.write((char*)&itPozition->count, sizeof(unsigned int));

				fileWrite.write((char*)&itPozition->presence, sizeof(bool));

				fileWrite.write((char*)&itPozition->screenDiagonal, sizeof(unsigned short));
				fileWrite.write((char*)&itPozition->ramMemory, sizeof(unsigned short));

				stringSize = itPozition->name.length() + 1;
				fileWrite.write((char*)&stringSize, sizeof(size_t));
				fileWrite.write(itPozition->name.c_str(), stringSize);

				stringSize = itPozition->company.length() + 1;
				fileWrite.write((char*)&stringSize, sizeof(size_t));
				fileWrite.write(itPozition->company.c_str(), stringSize);

				stringSize = itPozition->OS.length() + 1;
				fileWrite.write((char*)&stringSize, sizeof(size_t));
				fileWrite.write(itPozition->OS.c_str(), stringSize);

				++itPozition;
			}
			
			++it;
		}

		fileWrite.close();
	}

	void OrderStorage::Read()
	{
		ifstream fileRead(m_path, std::ios::binary);

		size_t size{};
		fileRead.read((char*)&size, sizeof(size_t));

		for (size_t i{}; i < size; i++)
		{
			Order order{};
			fileRead.read((char*)&order.id, sizeof(unsigned long));

			size_t stringSize{};
			fileRead.read((char*)&stringSize, sizeof(size_t));
			char* tempStr = new char[stringSize] {};
			fileRead.read(tempStr, stringSize);
			order.clientPhone = tempStr;
			delete[] tempStr;

			fileRead.read((char*)&order.date, sizeof(Order::Date));

			fileRead.read((char*)&order.status, sizeof(Order::Status));

			size_t sizePozition{};
			fileRead.read((char*)&sizePozition, sizeof(size_t));
			for (size_t j{}; j < sizePozition; j++)
			{
				Product product{};

				fileRead.read((char*)&product.id, sizeof(unsigned long));
				fileRead.read((char*)&product.cost, sizeof(unsigned long));

				fileRead.read((char*)&product.count, sizeof(unsigned int));

				fileRead.read((char*)&product.presence, sizeof(bool));

				fileRead.read((char*)&product.screenDiagonal, sizeof(unsigned short));
				fileRead.read((char*)&product.ramMemory, sizeof(unsigned short));

				size_t NstringSize{};
				fileRead.read((char*)&NstringSize, sizeof(size_t));
				char* NtempStr = new char[NstringSize] {};
				fileRead.read(NtempStr, NstringSize);
				product.name = NtempStr;
				delete[] NtempStr;

				fileRead.read((char*)&NstringSize, sizeof(size_t));
				NtempStr = new char[NstringSize] {};
				fileRead.read(NtempStr, NstringSize);
				product.company = NtempStr;
				delete[] NtempStr;

				fileRead.read((char*)&NstringSize, sizeof(size_t));
				NtempStr = new char[NstringSize] {};
				fileRead.read(NtempStr, NstringSize);
				product.OS = NtempStr;
				delete[] NtempStr;

				order.PozitionsList.push_back(product);
			}

			m_orderList.push_back(order);
		}

		fileRead.close();
	}

	void OrderStorage::Remove(int index)
	{
		m_orderList.erase(m_orderList.begin() + index);
		Write();
	}

	void OrderStorage::Add(Order order)
	{
		m_orderList.push_back(order);
		Write();
	}

	size_t OrderStorage::GetSize() { return m_orderList.size(); }

	Order& OrderStorage::operator[](int index) { return m_orderList[index]; }
}