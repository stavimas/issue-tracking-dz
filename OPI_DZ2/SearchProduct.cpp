#include "SearchProduct.h"
#include <iostream>
#include <iomanip>

namespace Shop
{
	void SearchProduct(ProductStorage& products)
	{
        string fieldOfSearch;

        std::cout << "������� �������� ������ ��������� ��� ��� ����� -> ";
        std::cin.ignore();
        std::getline(std::cin, fieldOfSearch);
        std::cout << "\n";

        int fieldOfSearchLength = fieldOfSearch.length();
        int arraySize = products.GetSize();

        bool* indexes = new bool[products.GetSize()]{};

        for (int i{}; i < products.GetSize(); ++i)
        {
            indexes[i] = false;
        }

        for (int i{}; i < products.GetSize(); ++i)
        {
            if (products[i].name.length() >= fieldOfSearchLength - 1)
            {
                for (int j = 0; j < (products[i].name.length() - fieldOfSearchLength + 1); ++j)
                {
                    string temp{};
                    for (int k{}; k < fieldOfSearchLength; ++k)
                    {
                        temp += (products[i].name)[j + k];
                    }

                    if (temp == fieldOfSearch)
                    {
                        indexes[i] = true;
                        break;
                    }
                    temp.clear();
                }
            }
        }
        vector <Product> tempArray{};
        for (int i{}; i < arraySize; ++i)
        {
            if (indexes[i])
            {
                tempArray.push_back(products[i]);
            }
        }
        arraySize = tempArray.size();

        if (arraySize == 0)
        {
            std::cout << "���������� �� �������!";
            std::cout << "\n\n";
        }
        else
        {
            std::cout << std::setw(10) << "id | " << std::setw(21) << "Name | " << std::setw(16) << "Company | "
                << std::setw(13) << "Ram memory | " << std::setw(11) << "OS | " << std::setw(17) << "Screen diagonal | "
                << std::setw(9) << "Cost | " << std::setw(8) << "Count | " << std::setw(10) << "Presence |" << std::endl;

            for (int i{}; i < 116; ++i)
            {
                std::cout << "-";
            }

            std::cout << std::endl;

            for (int i{}; i < arraySize; ++i)
            {
                std::cout << std::setw(7) << tempArray[i].id << " | " << std::setw(18) << tempArray[i].name << " | "
                    << std::setw(13) << tempArray[i].company << " | " << std::setw(10) << tempArray[i].ramMemory << " | "
                    << std::setw(8) << tempArray[i].OS << " | " << std::setw(15) << tempArray[i].screenDiagonal << " | "
                    << std::setw(6) << tempArray[i].cost << " | " << std::setw(5) << tempArray[i].count << " | ";

                if (tempArray[i].presence)
                {
                    std::cout << std::setw(10) << "�� |";
                }
                else
                {
                    std::cout << std::setw(10) << "��� |";
                }

                std::cout << std::endl;
            }

            for (int i{}; i < 116; ++i)
            {
                std::cout << "-";
            }

            std::cout << std::endl;
            std::cout << "\n\n";
        }
	}
}