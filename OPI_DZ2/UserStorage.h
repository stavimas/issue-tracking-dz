#ifndef USER_STORAGE_H
#define USER_STORAGE_H
#include "User.h"
#include <string>
#include <vector>

using std::string;
using std::vector;

namespace Shop
{
	class UserStorage
	{
	public:

		UserStorage();
		UserStorage(string path);

		void Write();
		void Read();

		void Add(User user);
		void Remove(int index);

		size_t GetSize();

		User& operator[](int index);

	private:
		string m_path{};
		vector <User> m_userList{};
	};
}

#endif // !USER_STORAGE_H
