#include "Menu.h"
#include "InputError.h"
#include <iostream>

namespace Shop
{
	Menu::Menu(OrderStorage& orderStorage, UserStorage& userStorage, ProductStorage& productStorage)
	{
		m_orderStorage = orderStorage;
		m_userStorage = userStorage;
		m_productStorage = productStorage;
	}

	void Menu::Draw()
	{
		system("cls");

		unsigned short command = 1;

		while (command != 0)
		{
			std::cout << "��������� ����������, ������������ ��� � �������� �����������!\n";
			std::cout << "--------------------------------------------------------------\n";
			std::cout << "1. ����� ��� ������������\n";
			std::cout << "2. ����� ��� �������������\n\n";
			std::cout << "0. �����\n";
			std::cout << "---------------------------\n";
			std::cout << "-> ";
			std::cin >> command;

			bool flag = false;
			while (flag == false)
			{
				try
				{
					if (command > 2)
					{
						throw InputError("\n������: ������������ ����\n\n");
					}

					else { flag = true; }
				}

				catch (InputError& exception)
				{
					std::cerr << exception.what() << "-> ";
					std::cin >> command;
				}
			}

			if (command == 1)
			{
				system("cls");

				m_role = User::Role::CLIENT;

				unsigned short clientCommand = 1;

				while (clientCommand != 0)
				{
					std::cout << "1. ������ ���� �������\n";
					std::cout << "2. �������� �������\n";
					std::cout << "3. ����� ����� �� ��������\n";
					std::cout << "4. ���������� ������� �������\n\n";
					std::cout << "0. �����\n";
					std::cout << "-----------------------------\n";
					std::cout << "-> ";
					std::cin >> clientCommand;
					
					bool flag = false;
					while (flag == false)
					{
						try
						{
							if (clientCommand > 4)
							{
								throw InputError("\n������: ������������ ����\n\n");
							}

							else { flag = true; }
						}

						catch (InputError& exception)
						{
							std::cerr << exception.what() << "-> ";
							std::cin >> clientCommand;
						}
					}

					if (clientCommand == 1)
					{
						system("cls");

						unsigned int clientComInShow = 1;

						while (clientComInShow != 0)
						{
							ShowProductList(m_productStorage);

							std::cout << "\n1. ������������� ������ �������\n";
							std::cout << "2. ������������� ������ �������\n";
							std::cout << "\n0. �����\n";
							std::cout << "-------------------------------\n-> ";


							std::cin >> clientComInShow;

							bool flag = false;
							while (flag == false)
							{
								try
								{
									if (clientComInShow > 2)
									{
										throw InputError("\n������: ������������ ����\n\n");
									}

									else { flag = true; }
								}

								catch (InputError& exception)
								{
									std::cerr << exception.what() << "-> ";
									std::cin >> clientComInShow;
								}
							}

							if (clientComInShow == 1)
							{
								SortProduct(m_productStorage);
							}
							else if (clientComInShow == 2)
							{
								FilterProduct(m_productStorage);
							}

							if (clientComInShow == 0) { system("cls"); }
						}
					}
					else if (clientCommand == 2)
					{
						system("cls");
						Payment(m_productStorage, m_orderStorage);
					}
					else if (clientCommand == 3)
					{
						system("cls");
						SearchProduct(m_productStorage);
					}
					else if (clientCommand == 4)
					{
						system("cls");
						OrderHistory(m_orderStorage);
					}
					else if (clientCommand == 0)
					{
						system("cls");
					}
				}
			}

			else if (command == 2)
			{
				system("cls");

				m_authorizedId = LogIn(m_userStorage);

				for (size_t i{}; i < m_userStorage.GetSize(); ++i)
				{
					if (m_userStorage[i].id == m_authorizedId)
					{
						m_role = m_userStorage[i].role;
						break;
					}
				}

				if (m_role == User::ADMIN)
				{
					system("cls");

					unsigned short adminCommand = 1;

					while (adminCommand != 0)
					{
						std::cout << "1. ������ �� ������� �������\n";
						std::cout << "2. �������� ������ ���������� �������\n";
						std::cout << "3. �������� ������ ���� �������\n\n";
						std::cout << "0. �����\n";
						std::cout << "-------------------------------------\n";
						std::cout << "-> ";
						std::cin >> adminCommand;

						bool flag = false;
						while (flag == false)
						{
							try
							{
								if (adminCommand > 3)
								{
									throw InputError("������: ������������ ����\n\n");
								}

								else { flag = true; }
							}

							catch (InputError& exception)
							{
								std::cerr << exception.what() << "-> ";
								std::cin >> adminCommand;
							}
						}

						if (adminCommand == 1)
						{
							system("cls");
							unsigned short listCommand = 1;

							while (listCommand != 0)
							{
								ShowProductList(m_productStorage);

								std::cout << "\n1. �������� ����� �����\n";
								std::cout << "2. �������� ������ ������\n";
								std::cout << "3. ������������� ������ �������\n";
								std::cout << "4. ������������� ������ �������\n\n";
								std::cout << "0. �����\n";
								std::cout << "-------------------------\n";
								std::cout << "-> ";
								std::cin >> listCommand;

								bool flag = false;
								while (flag == false)
								{
									try
									{
										if (listCommand > 4)
										{
											throw InputError("������: ������������ ����\n\n");
										}

										else { flag = true; }
									}

									catch (InputError& exception)
									{
										std::cerr << exception.what() << "-> ";
										std::cin >> adminCommand;
									}
								}

								if (listCommand == 1)
								{
									system("cls");
									AddProductList(m_productStorage);
								}
								else if (listCommand == 2)
								{
									system("cls");
									ShowProductList(m_productStorage);
									EditProductList(m_productStorage);
								}
								else if (listCommand == 3)
								{
									system("cls");
									SortProduct(m_productStorage);
								}
								else if (listCommand == 4)
								{
									system("cls");
									FilterProduct(m_productStorage);
								}
							}

							if (listCommand == 0){ system("cls"); }
						}
						else if (adminCommand == 2)
						{
							system("cls");
							ShowBuyedProducts(m_orderStorage);
						}
						else if (adminCommand == 3)
						{
							system("cls");
							ShowOrderList(m_orderStorage);
						}
						else if (adminCommand == 0)
						{
							system("cls");
						}
					}
				}
			}
		}

		system("cls");
	}
}