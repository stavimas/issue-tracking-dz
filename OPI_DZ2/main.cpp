#include <iostream>
#include "Menu.h"


using namespace Shop;

int main()
{
	setlocale(LC_ALL, "RUS");

	OrderStorage orderStorage = OrderStorage("Orders.dat");
	ProductStorage productStorage = ProductStorage("Products.dat");
	UserStorage userStorage = UserStorage("Users.dat");

	Menu menu = Menu(orderStorage, userStorage, productStorage);

	menu.Draw();

	return 0;
}